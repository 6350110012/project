import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upload_image1/Team.dart';
import 'package:upload_image1/home/authentication.dart';
import 'package:upload_image1/fi_chart.dart';
import 'package:upload_image1/home/login.dart';
import 'package:upload_image1/home/test.dart';
import 'package:upload_image1/main.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text fields' controllers
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;




  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _Bakery =
  FirebaseFirestore.instance.collection('Bakery');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
    }

    await showModalBottomSheet(
      backgroundColor: Colors.amber[200],
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 40,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column( 
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 50),
                    child: Image(image:AssetImage("asset/images/Add.png"),width: 250, ),
                  )
                ],),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: TextField(
                    controller: _nameController,
                    decoration: const InputDecoration(labelText: 'Name'),
                  ),
                ),
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _priceController,
                  decoration: const InputDecoration(
                    labelText: 'Price',
                  ),
                ),
                const SizedBox(
                  height: 60,
                ),
                SizedBox(width: 250,height: 50,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 100),
                    child: ElevatedButton(style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all(Colors.brown[500]),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ))),
                      child: Text(action == 'create' ? 'Create'  : 'Update',style: TextStyle(color: Colors.amber,fontSize: 18)),
                      onPressed: () async {
                        final String name = _nameController.text ;
                        final double? price = double.tryParse(_priceController.text);
                        if (name != null && price != null) {
                          if (action == 'create') {
                            // Persist a new product to Firestore
                            await _Bakery
                                .add({"name": name, "price": price,})
                                .then((value) => print("Product Added"))
                                .catchError((error) =>
                                print("Failed to add product: $error"));
                          }

                          if (action == 'update') {
                            // Update the product
                            await _Bakery
                                .doc(documentSnapshot?.id)
                                .update({"name": name, "price": price,})
                                .then((value) => print("Product Updated"))
                                .catchError((error) =>
                                print("Failed to update product: $error"));
                          }

                          // Clear the text fields
                          _nameController.text = '';
                          _priceController.text = '';


                          // Hide the bottom sheet
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _Bakery
        .doc(productId)
        .delete()
        .then((value) => print("Product Deleted"))
        .catchError((error) => print("Failed to delete product: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a product')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor:Colors.amber[100] ,
      appBar: AppBar(backgroundColor: Colors.amber[200],
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.brown,
              size: 30,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (contex) => test(title: '',)),
              ));
            },
          )
        ],
        title: const Text('BAKERY',style: TextStyle(color: Colors.brown)),
      ),

      // Using StreamBuilder to display all products from Firestore in real-time
      body:
      StreamBuilder(
        stream: _Bakery.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                streamSnapshot.data!.docs[index];
                return Card(color: Colors.brown[400],
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    leading: Image( image: AssetImage("asset/images/chef.png")),
                    title: Text(documentSnapshot['name'],style: TextStyle( color: Colors.amber[200])),
                    subtitle: Text(documentSnapshot['price'].toString(),style: TextStyle(color:Colors.amber[100])),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(Icons.edit,color: Colors.white54),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                              icon: const Icon(Icons.delete,color: Colors.white54),
                              onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add,color: Colors.brown),
        backgroundColor: Colors.amber[200],

      ),


      drawer:Drawer(backgroundColor: Colors.amber[100],
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:50 ),
              child: Image.asset('asset/images/1.png',width: 100,),
            ),

            ListTile(
              leading: Icon(Icons.account_circle_sharp,color:Colors.brown,
              ),
              title: Text(  user.email,style:GoogleFonts.lato(textStyle: TextStyle(fontSize: 18,color: Colors.brown))),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => MyApp(),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),



            ListTile(
              leading: Icon(Icons.bakery_dining_rounded,color:Colors.brown,
              ),
              title: const Text('Team',style: TextStyle(fontSize: 17,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ProfileDeveloper(title: '',),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),
            ListTile(
              leading: Icon(Icons.area_chart,color:Colors.brown,
              ),
              title: const Text('Chart',style: TextStyle(fontSize: 17,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => FiChartPage(),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),
            ListTile(
              leading: Icon(Icons.logout,color:Colors.brown,
              ),
              title: const Text('LOG OUT ',style: TextStyle(fontSize: 15,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => test(title: ""),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),

          ],
        ),
      ),
    );
  }
}
