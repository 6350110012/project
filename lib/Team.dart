import 'dart:io';
import 'package:flutter/material.dart';
import 'package:upload_image1/Home_.dart';


class ProfileDeveloper extends StatefulWidget {
  const ProfileDeveloper({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ProfileDeveloper> createState() => _ProfileDeveloperState();
}

class _ProfileDeveloperState extends State<ProfileDeveloper> {



  @override


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      body: Container(
      child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
      Padding(
      padding: const EdgeInsets.only(top:60,bottom: 30,left: 20),
      child: Image.asset("asset/images/Team-1.png", width: 400),
      ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: CircleAvatar(
                backgroundImage: AssetImage("asset/images/green.jpg"),
                radius: 55,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text("Nuchanard Viriyapuree",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Colors.brown,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 7, left: 40),
                  child: Text("6350110012",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.brown,
                      )),
                ),
              ],
            ),
          ],
        ),

        Padding(
          padding: const EdgeInsets.only(top:20,bottom: 30),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 25,),
                child: CircleAvatar(
                  backgroundImage: AssetImage("asset/images/jom.jpg"),
                  radius: 55,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text("Panitan Kuedoy",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.brown,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 7, left: 40),
                    child: Text("6350110013",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Colors.brown,
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SizedBox(
            width: 270,
            height: 60,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.brown[900]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "BACK",
                  style: TextStyle(fontSize: 20, color: Colors.amber[200]),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context){
                        return HomePage();
                      })
                  );
                },
              ),
            ),
          ),
        )
    ],
    ),
      ),

    );
  }
}
