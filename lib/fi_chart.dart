import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class FiChartPage extends StatefulWidget {
  const FiChartPage({Key? key}) : super(key: key);

  @override
  _FiChartPageState createState() => _FiChartPageState();
}

class _FiChartPageState extends State<FiChartPage> {
  List<SalesData> data = [
    SalesData("Jan", 35),
    SalesData("Feb", 28),
    SalesData("Mar", 32),
    SalesData("Apr", 40),
    SalesData("May", 22),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: Container(
       color:Colors.amber[300] ,
       child:SfCartesianChart(
         primaryXAxis: CategoryAxis(),
         title: ChartTitle(text: "205"),
         legend: Legend(isVisible: true),
         tooltipBehavior: TooltipBehavior(enable: true),backgroundColor: Colors.amber[100],
         series: <ChartSeries<SalesData, String>>[
           LineSeries<SalesData,String>(color: Colors.brown[300],
             dataSource: data,
             xValueMapper: (SalesData sales, _) => sales.month,
             yValueMapper: (SalesData sales, _) => sales.sales,
             name: "BAKERY",
             dataLabelSettings: DataLabelSettings(isVisible: true,color: Colors.brown),

           ),
         ],

       ),
     ),

    );

  }
}
class SalesData{
  final String month;
  final double sales;

  SalesData(this.month,this.sales);

}