import 'package:flutter/material.dart';
import 'package:upload_image1/Home_.dart';
import 'package:upload_image1/home/authentication.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';




class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[100],
      body: ListView(
        children: <Widget>[
          // logo
          Padding(
            padding: const EdgeInsets.only(top: 70),
            child: Column(children: [
              Image(image: AssetImage("asset/images/login.png"),width: 300),
            ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: LoginForm(),
          ),
        ],
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String? email;
  String? password;

  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(top:10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            // email
            TextFormField(
              // initialValue: 'Input text',
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.email_outlined,color: Colors.brown[800],),
                labelText: 'Email',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    const Radius.circular(100.0),
                  ),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              onSaved: (val) {
                email = val;
              },
            ),
            SizedBox(
              height: 20,
            ),

            // password
            Padding(
              padding: const EdgeInsets.only(),
              child: TextFormField(
                // initialValue: 'Input text',
                decoration: InputDecoration(
                  labelText: 'Password' ,
                  prefixIcon: Icon(Icons.lock_outline,color: Colors.brown[800]),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      const Radius.circular(100.0),
                    ),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    },
                    child: Icon(
                      _obscureText ? Icons.visibility_off : Icons.visibility,color: Colors.brown[800]
                    ),
                  ),
                ),
                obscureText: _obscureText,
                onSaved: (val) {
                  password = val;
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),

            SizedBox(height: 40),

            SizedBox(
              height: 50,
              width: 250,
              child: ElevatedButton(
                onPressed: () {
                  // Respond to button press

                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();

                    AuthenticationHelper()
                        .signIn(email: email!, password: password!)
                        .then((result) {
                      if (result == null) {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => HomePage()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(
                            result,
                            style: TextStyle(fontSize: 20),
                          ),
                        ));
                      }
                    });
                  }
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.brown[700]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "LOGIN",
                  style: TextStyle(fontSize: 20, color: Colors.amber[200]),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: SizedBox(width: 250, height: 50,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.brown[900]),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ))),
                  child: Text(
                    "LOGIN WITH GOOGLE",
                    style: TextStyle(fontSize: 18, color: Colors.amber[200]),
                  ),
                  onPressed: () {
                    AuthenticationHelper().signInWithGoogle().then((result){
                      if (result == null) {
                        Navigator.pushReplacement(
                            context, MaterialPageRoute(builder: (context) => HomePage()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(
                            result,
                            style: TextStyle(fontSize: 16),
                          )
                        ));
                      }
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

