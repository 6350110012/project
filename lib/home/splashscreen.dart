import 'dart:async';
import 'package:flutter/material.dart';

import 'test.dart';

class SplashScreen extends StatefulWidget{
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
  
  }
  class _SplashScreenState extends State<SplashScreen> {
    @override
     void initState(){
      super.initState();
      Timer(Duration(seconds: 5), () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => test(title: '',),
        ),
        );
      }
      );
    }
    Widget build(BuildContext context) {
      return Scaffold(
        backgroundColor: Colors.amber[100],
        body: Center(
          child: Padding(
            padding: const EdgeInsets.only(right: 15,left: 10,bottom: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("asset/images/bakery.png",
                  width: 900 ,
                )
              ],
            ),
          ),
        ),
      );
    }
  }
  
  